<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Transaction;
use App\Cart;

class TransactionController extends Controller
{
    public function store()
    {
        //Untuk memulai manual transaction
        //dan dapat mengkontrol secara penuh
        //atas Rollbacks dan Commits

//        dd(auth()->user()->transaction()->create(request()->all())->details());

        DB::beginTransaction();
        try{
            auth()->user()->transactions()
                ->create(request()->all())
                ->details()
                ->createMany(Cart::all()->map(function ($cart){
                    //Diambil All dulu, terus dimap, map return collection
                    //Convert ke array agar bisa dipake didlm createMany
                    //https://laravel.com/docs/6.x/eloquent-relationships
                    return [
                        'item_id' => $cart->item_id,
                        'quantity' => $cart->quantity,
                        'subtotal' => $cart->quantity * $cart->item->price
                    ];
                })->toArray());
            DB::table('carts')->delete();

            DB::commit();
        }catch(Exception $e){
            DB::rollback();
        }

        return redirect()->route('transaction.show', Transaction::latest()->first());
    }

    public function index()
    {
        $transactions = Transaction::latest()->get();
        return view('transaction.index', compact('transactions'));
    }

    public function show(Transaction $transaction)
    {
        return view('transaction.show', compact('transaction'));
    }
}
