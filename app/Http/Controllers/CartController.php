<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;

class CartController extends Controller
{
    public function store()
    {
        if(request()->item_id) {
            Cart::create(request()->all());
        }else{
            return redirect()->back()->with('error','Terjadi kesalahan.');
        }
        return redirect()->back()->with('success','Sukses ditambahkan.');
    }

    public function update(Cart $cart)
    {
        $cart->update(request()->all());
        return redirect()->back();
    }

    public function destroy(Cart $cart)
    {
        $cart->delete();
        return redirect()->back();
    }
}
