<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\TransactionDetail;


class Transaction extends Model
{
    protected $guarded = [];

    //Ganti
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function details()
    {
        return $this->hasMany(TransactionDetail::class);
    }

}
