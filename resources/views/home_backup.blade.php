@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-4 mb-4">
            <div class="card">
                <div class="card-header">Pilih Barang</div>

                <div class="card-body">
                    <form action="">
                        <input type="hidden" name="item_id" id="itemId">

                        <div class="form-group">
                            <div class="input-group">
                                <input type="text"
                                       class="form-control"
                                       id="itemName"
                                       placeholder="Pilih Barang..."
                                       readonly>
                                <div class="input-group-append">
                                    <button type="button"
                                            class="btn btn-outline-primary"
                                            data-toggle="modal"
                                            data-target="#pilihBarang"
                                    >Pilih</button>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="pilihBarang">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Pilih Barang</h5>
                                        <button type="button" class="close" data-dismiss="modal">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <table class="table table-striped">
                                            <thead class="thead-dark">
                                                <th scope="col">#</th>
                                                <th scope="col">Nama Barang</th>
                                                <th scope="col">Kategori</th>
                                                <th scope="col">Foto</th>
                                                <th scope="col">Harga</th>
                                                <th scope="col">Stok</th>
                                                <th scope="col">Opsi</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>Sabun</td>
                                                    <td>Makanan</td>
                                                    <td>
                                                        <img src="{{asset('')}}" alt="Loading...">
                                                    </td>
                                                    <td>10000</td>
                                                    <td>100</td>
                                                    <td>
                                                        <button type="button"
                                                                class="btn btn-sm btn-primary"
                                                                data-dismiss="modal"
                                                        >Pilih</button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <input type="number"
                                       min="1" max="2"
                                       value="1"
                                       class="form-control"
                                       id="itemQty"
                                       name="quantity"
                                       placeholder="Masukkan jumlah..."
                                       required>
                                <div class="input-group-append">
                                    <span class="input-group-text">Unit</span>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary float-right">Tambah</button>

                    </form>
                </div>

            </div>
        </div>

        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Pembayaran</div>

                <div class="card-body">

                </div>
            </div>
        </div>

    </div>

    <table class="table table-striped">
        <thead class="thead-dark">
            <th scope="col">#</th>
            <th scope="col">Nama Barang</th>
            <th scope="col">Kategori</th>
            <th scope="col">Foto</th>
            <th scope="col">Harga</th>
            <th scope="col">Jumlah</th>
            <th scope="col">Subtotal</th>
            <th scope="col">Opsi</th>
        </thead>
        <tbody>
            <tr>
                <th scope="row">1</th>
                <td>Sabun</td>
                <td>Makanan</td>
                <td>
                    <img src="{{asset('')}}" alt="Loading...">
                </td>
                <td>Rp 10000</td>
                <td>10</td>
                <td>Rp 100000</td>
                <td>
                    <button class="btn btn-sm btn-primary">Ubah</button>
                    <form action="" class="d-inline">
                        <button type="submit" class="btn btn-sm btn-primary">Delete</button>
                    </form>
                </td>
            </tr>
        </tbody>
    </table>

</div>
@endsection
