<?php

use Illuminate\Database\Seeder;
use App\Item;

class ItemsSeeder extends Seeder
{
    public function run()
    {
        for ($i=0; $i<5; $i++){
            Item::create([
                'item_category_id' => ($i+1),
                'name' => 'Test'.($i+1),
                'price' => 1000*($i+1),
                'stock' => 20*($i+1),
                'image' => 'Image'.($i+1)
            ]);
        }
    }
}
