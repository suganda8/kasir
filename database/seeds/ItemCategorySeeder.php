<?php

use Illuminate\Database\Seeder;
use App\ItemCategory;

class ItemCategorySeeder extends Seeder
{
    public function run()
    {
        $categories = [
            'Makanan',
            'Minuman',
            'Alat Tulis',
            'Alat Dapur',
            'Pembersih'
        ];

        foreach($categories as $category){
            ItemCategory::create([
                'name' => $category
            ]);
        }
    }
}
